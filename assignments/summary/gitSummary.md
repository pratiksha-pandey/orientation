**Git**
Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

Some git commands are:
**git init**
This command turns a directory into an empty Git repository. This is the first step in creating a repository. After running git init, adding and committing files/directories is possible.

**git add**
Adds files in the to the staging area for Git. Before a file is available to commit to a repository, the file needs to be added to the Git index (staging area). There are a few different ways to use git add, by adding entire directories, specific files, or all unstaged files.

**git commit**
Record the changes made to the files to a local repository. For easy reference, each commit has a unique ID.

It’s best practice to include a message with each commit explaining the changes made in a commit. Adding a commit message helps to find a particular change or understanding the changes.

**git status**
This command returns the current state of the repository.

git status will return the current working branch. If a file is in the staging area, but not committed, it shows with git status. Or, if there are no changes it’ll return nothing to commit, working directory clean.

**git config**
With Git, there are many configurations and settings possible. git config is how to assign these settings. Two important settings are user user.name and user.email. These values set what email address and name commits will be from on a local computer. With git config, a --global flag is used to write the settings to all repositories on a computer. Without a --global flag settings will only apply to the current repository that you are currently in.

**Git basic workflow**

1. Host central repositories
2. Clone the central repository
3. Make changes and commit
4. Push new commits to central repository
5. Managing conflicts

**Git internals**
Terminology associated with the general concepts of how git works:


- Working Directory: your project files.
- Staging Area: a file that tracks the changes to your project files.
- Repository: the location where your project files are stored.

The git subcommands are generally split into one of two groups:


- Porcelain: the user-friendly interface (e.g. git checkout, git pull etc.)
- Plumbing: low-level interface (e.g. git cat-file, git rev-parse etc.)

The .git directory: 
When you start a new project that you want to use version control for, you’ll typically run the git init subcommand.

The two most important concepts in git are: references and objects.
References:
Git is built upon the simple premise of using ‘pointers’ to data, and these pointers are typically referred to as ‘references’ (or ‘refs’ for short).
This is what the .git/refs directory stores: references.

Object Types:
There are four main types of objects in git:
- commit
- tree
- blob
- tag




